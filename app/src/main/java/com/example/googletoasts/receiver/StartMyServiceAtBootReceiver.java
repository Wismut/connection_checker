package com.example.googletoasts.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.example.googletoasts.service.GoogleConnectionCheckerService;

public class StartMyServiceAtBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, GoogleConnectionCheckerService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(serviceIntent);
        } else {
            context.startService(serviceIntent);
        }
        Log.i("Autostart", "started");
//        serviceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        context.startService(serviceIntent);
    }
}
