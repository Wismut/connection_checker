package com.example.googletoasts.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.example.googletoasts.MainActivity;

import java.net.InetAddress;

public class GoogleConnectionCheckerService extends Service {
    private static final String TAG = "MyService";
    boolean isInternetAvailable = false;

    @Override
    public IBinder onBind(Intent intent) {
        Toast.makeText(this, "Toast", Toast.LENGTH_LONG).show();
        return null;
    }

    public void onDestroy() {
        Toast.makeText(this, "My Service Stopped", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public void onStart(Intent intent, int startid) {
        Toast.makeText(GoogleConnectionCheckerService.this, "Service onStart", Toast.LENGTH_LONG).show();
        Intent intents = new Intent(getBaseContext(), MainActivity.class);
        intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intents);
        Toast.makeText(GoogleConnectionCheckerService.this, "Main activity started", Toast.LENGTH_LONG).show();
        Handler handler = new Handler();
        checkInternet();
//        while (!isInternetAvailable) {
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    checkInternet();
//                    Toast.makeText(GoogleConnectionCheckerService.this, "Checking internet", Toast.LENGTH_LONG).show();
//                }
//            }, 2000);
//        }
        Toast.makeText(GoogleConnectionCheckerService.this, "Internet is present " + isInternetAvailable, Toast.LENGTH_LONG).show();
        Log.d(TAG, "onStart");
    }

    private void checkInternet() {
//        try {
//            InetAddress ipAddr = InetAddress.getByName("google.com");
//            return ipAddr.
//        } catch (Exception e) {
//            return false;
//        }
        final Thread thread = new Thread() {
            @Override
            public void run() {
                while (!isInternetAvailable) {
                    ConnectivityManager connectivityManager
                            = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    if (connectivityManager == null) {
                        isInternetAvailable = false;
                    } else {
                        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                        isInternetAvailable = activeNetworkInfo != null && activeNetworkInfo.isConnected();
                    }
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        Log.d("Thread sleep exception", e.getLocalizedMessage());
                    }
                }
            }
        };
        thread.start();
    }
}
